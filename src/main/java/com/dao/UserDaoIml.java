package com.dao;

import com.model.User;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Repository("userDao")
public class UserDaoIml implements UserDao {
    private final String url = "/Users/viktor/Desktop/test.xls";
    private FileInputStream fis;
    private FileOutputStream fos;

    private List<User> users = new ArrayList<User>();

    public List<User> getAllUsers() {
        return users;
    }

    public void addUser(User newUser) throws IOException {

        fis = new FileInputStream(url);
        Workbook workbook = new HSSFWorkbook(fis);
        Sheet sheet = workbook.getSheetAt(0);

        int numberOfRows = sheet.getPhysicalNumberOfRows();
        System.out.println(numberOfRows);

        System.out.println(newUser);

        Row row = sheet.createRow(numberOfRows);

        row.createCell(0).setCellValue(newUser.getName());
        row.createCell(1).setCellValue(newUser.getAge());

        fos = new FileOutputStream(url);
        workbook.write(fos);
        fos.close();

    }
}

