<%@ taglib prefix="spring" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Users</title>
</head>
<body>

<form:form method="POST" action="/web/users/add">

    <p>Name</p>
    <form:input path="name"/>

    <p>Age</p>
    <form:input path="age"/>

    <p>
        <input type="submit" value="Submit"/>
    </p>

</form:form>
</body>
</html>
