package com.service;

import com.model.User;

import java.io.IOException;
import java.util.List;

public interface UserService {

    List<User> getAllUsers();

    void addUser(User newUser) throws IOException;
}
