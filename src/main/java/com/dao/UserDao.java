package com.dao;

import com.model.User;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface UserDao {

    List<User> getAllUsers();

    void addUser(User newUser) throws IOException;
}
