<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Users</title>
</head>
<body>
<ul>
    <li>name: ${name}</li>
</ul>
<ul>
    <li>age: ${age}</li>
</ul>

</body>
<a href="<spring:url value="/users/init"/>">back</a>
</html>
